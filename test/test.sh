#!/bin/sh

value=$(curl -sf http://localhost:5521/metrics | awk '/^probe_success/ {print $2}')
case "$value" in
    1)
        echo "success!" >&2
        exit 0
        ;;

    0)
        echo "probe has failed" >&2
        exit 2
        ;;
esac

# No probe_success value, or connection error. Retry.
exit 1
