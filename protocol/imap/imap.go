package imap

import (
	"bufio"
	"context"
	"crypto/tls"
	"encoding/base64"
	"fmt"
	"io"
	"log"
	"net"
	"regexp"
	"strconv"
	"strings"

	"git.autistici.org/ai3/tools/service-prober/protocol/dns"
)

// Conn knows just enough about the IMAP protocol to run a simple
// prober (not enough for a real, functional client).
type Conn struct {
	lineReadWriter

	conn       net.Conn
	cmdCounter int
}

// Dial a remote IMAP server (with mandatory TLS). The IMAP connection
// is bound to the lifetime of the Context.
func Dial(ctx context.Context, addr string, conf *tls.Config, log *log.Logger, dnsMap map[string]string) (*Conn, error) {
	dialer := dns.NewDNSOverrideDialer(dnsMap, conf)
	nc, err := dialer.DialTLSContext(ctx, "tcp", addr)
	if err != nil {
		return nil, err
	}

	// Set the context deadline on the connection.
	if deadline, ok := ctx.Deadline(); ok {
		nc.SetDeadline(deadline)
	}

	// Ensure that canceling the context will immediately close
	// the connection.
	go func() {
		<-ctx.Done()
		nc.Close()
	}()

	log.Printf("established IMAP+SSL connection to %s", nc.RemoteAddr().String())

	c := &Conn{
		lineReadWriter: newLoggingBufferedReadWriter(
			&bufferedReadWriter{
				Reader: bufio.NewReader(nc),
				Writer: nc,
			},
			log,
		),
		conn: nc,
	}

	// Read (and discard) the greeting line.
	if _, err := c.ReadLine(); err != nil {
		c.Close()
		return nil, err
	}

	return c, nil
}

// Close the IMAP connection.
func (c *Conn) Close() error {
	return c.conn.Close()
}

func (c *Conn) getToken() string {
	c.cmdCounter++
	return fmt.Sprintf("C%03d", c.cmdCounter)
}

func (c *Conn) writeCmd(cmd string) (string, error) {
	token := c.getToken()
	return token, c.WriteLine(fmt.Sprintf("%s %s", token, cmd))
}

func (c *Conn) readResponse(token string, process func(string) error) error {
	tokenSp := token + " "
	for {
		s, err := c.ReadLine()
		if err != nil {
			return err
		}

		if strings.HasPrefix(s, tokenSp) {
			status := s[len(tokenSp):]
			if strings.HasPrefix(status, "OK") {
				return nil
			}
			return fmt.Errorf("IMAP error: %s", status)
		} else if process != nil {
			if err := process(s); err != nil {
				return err
			}
		}
	}
}

func (c *Conn) cmd(cmd string, process func(string) error) error {
	token, err := c.writeCmd(cmd)
	if err != nil {
		return err
	}
	return c.readResponse(token, process)
}

// LoginSASL performs a login with SASL AUTH PLAIN mechanism.
func (c *Conn) LoginSASL(username, password string) error {
	var b []byte
	b = append(b, '\000')
	b = append(b, []byte(username)...)
	b = append(b, '\000')
	b = append(b, []byte(password)...)
	enc := base64.StdEncoding.EncodeToString(b)

	return c.cmd(fmt.Sprintf("AUTHENTICATE PLAIN\r\n%s", enc), func(line string) error {
		if strings.TrimSpace(line) != "+" && !strings.HasPrefix(line, "* ") {
			return fmt.Errorf("unexpected IMAP auth response: '%s'", line)
		}
		return nil
	})
}

// Select a mailbox.
func (c *Conn) Select(mbox string) error {
	return c.cmd(fmt.Sprintf("SELECT \"%s\"", mbox), nil)
}

// Search the current mailbox for a given query string.
func (c *Conn) Search(qstr string) ([]string, error) {
	var result []string
	err := c.cmd(fmt.Sprintf("UID SEARCH %s", qstr), func(line string) error {
		if strings.HasPrefix(line, "* SEARCH ") {
			values := strings.Split(line[9:], " ")
			result = append(result, values...)
		}
		return nil
	})
	return result, err
}

// Regexp used to parse the FETCH response line and know how many
// bytes to read for the message body.
var fetchRx = regexp.MustCompile(`\* \d+ FETCH \(.* RFC822 {(\d+)}\s*$`)

// Fetch a message, returns the whole rfc-822 body.
func (c *Conn) Fetch(uid string) (string, error) {
	var lines []string
	var sz int
	var inBody bool
	err := c.cmd(fmt.Sprintf("UID FETCH %s (RFC822)", uid), func(line string) error {
		if !inBody {
			if m := fetchRx.FindStringSubmatch(line); len(m) > 0 {
				i, err := strconv.Atoi(m[1])
				if err != nil {
					return fmt.Errorf("can't parse FETCH result: %s", line)
				}
				sz = i
				inBody = true
			}
		} else if inBody {
			lines = append(lines, line)
		}
		return nil
	})

	// Truncate message to expected size (should drop the final
	// closing parenthesis).
	return strings.Join(lines, "\r\n")[:sz], err
}

// Common IMAP message flags.
const (
	IMAPFlagDeleted = "\\Deleted"
	IMAPFlagSeen    = "\\Seen"
)

// Store new flags for a message.
func (c *Conn) Store(uid, flags string) error {
	return c.cmd(fmt.Sprintf("UID STORE %s +FLAGS (%s)", uid, flags), nil)
}

// Expunge the current mailbox.
func (c *Conn) Expunge() error {
	return c.cmd("EXPUNGE", nil)
}

// var (
// 	flagServer   = flag.String("server", "", "server (host:port)")
// 	flagUsername = flag.String("username", "", "username")
// 	flagPassword = flag.String("password", "", "password")
// )

// func imapMain() {
// 	flag.Parse()

// 	//ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
// 	//defer cancel()
// 	ctx, cancel := context.WithCancel(context.Background())
// 	go func() {
// 		time.Sleep(1 * time.Second)
// 		cancel()
// 	}()

// 	c, err := Dial(ctx, *flagServer, nil)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	defer c.Close()

// 	if err := c.LoginSASL(*flagUsername, *flagPassword); err != nil {
// 		log.Fatal(err)
// 	}

// 	if err := c.Select("INBOX"); err != nil {
// 		log.Fatal(err)
// 	}

// 	//msgs, err := c.search("HEADER Message-ID *")
// 	msgs, err := c.Search("HEADER From tre@investici.org")
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	// for _, id := range msgs {
// 	// 	if err := c.Store(id, IMAPFlagDeleted); err != nil {
// 	// 		log.Fatal(err)
// 	// 	}
// 	// }

// 	if len(msgs) > 3 {
// 		msg, err := c.Fetch(msgs[3])
// 		if err != nil {
// 			log.Fatal(err)
// 		}
// 		fmt.Printf("%s\n", msg)
// 	}

// 	if err := c.Expunge(); err != nil {
// 		log.Fatal(err)
// 	}
// }

type lineReadWriter interface {
	ReadLine() (string, error)
	WriteLine(string) error
}

type bufferedReadWriter struct {
	*bufio.Reader
	io.Writer
}

func (b *bufferedReadWriter) ReadLine() (string, error) {
	s, err := b.Reader.ReadString('\n')
	if err != nil {
		return "", err
	}
	return strings.TrimRight(s, "\r\n"), nil
}

func (b *bufferedReadWriter) WriteLine(s string) error {
	_, err := io.WriteString(b, s+"\r\n")
	return err
}

type loggingBufferedReadWriter struct {
	rw  *bufferedReadWriter
	log *log.Logger
}

func newLoggingBufferedReadWriter(rw *bufferedReadWriter, log *log.Logger) *loggingBufferedReadWriter {
	return &loggingBufferedReadWriter{
		rw:  rw,
		log: log,
	}
}

func (l *loggingBufferedReadWriter) ReadLine() (s string, err error) {
	s, err = l.rw.ReadLine()
	if err == nil {
		l.log.Printf("<<< %s", s)
	}
	return
}

func (l *loggingBufferedReadWriter) WriteLine(s string) error {
	l.log.Printf(">>> %s", s)
	return l.rw.WriteLine(s)
}
