package openvpn

import (
	"context"
	"time"

	"github.com/ooni/minivpn/extras/ping"
	"github.com/ooni/minivpn/vpn"
)

type PingOptions struct {
	Target  string
	Count   int
	Timeout time.Duration
}

// OpenVPNPinger sends and receives ICMP Echo packets over an OpenVPN connection. Returns a configured Pinger, and an error if the Pinger object cannot be properly initialized.
func NewPingerFromOptions(opts *vpn.Options, ctx context.Context, pingOpts *PingOptions) (*ping.Pinger, error) {
	tunnel := vpn.NewClientFromOptions(opts)
	if err := tunnel.Start(ctx); err != nil {
		return nil, err
	}

	pinger := ping.New(pingOpts.Target, tunnel)
	pinger.Count = pingOpts.Count
	pinger.Timeout = pingOpts.Timeout

	return pinger, nil
}
