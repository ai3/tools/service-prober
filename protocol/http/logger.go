package http

import (
	"bufio"
	"bytes"
	"log"
	"net/http"
	"time"
)

// Slight adaptation of https://github.com/ernesto-jimenez/httplogger.

type loggedRoundTripper struct {
	rt  http.RoundTripper
	log httpLogger
}

func (c *loggedRoundTripper) RoundTrip(request *http.Request) (*http.Response, error) {
	c.log.LogRequest(request)
	startTime := time.Now()
	response, err := c.rt.RoundTrip(request)
	duration := time.Since(startTime)
	c.log.LogResponse(request, response, err, duration)
	return response, err
}

// newLoggedTransport takes an http.RoundTripper and returns a new one that logs requests and responses
func newLoggedTransport(rt http.RoundTripper, debug *log.Logger) http.RoundTripper {
	return &loggedRoundTripper{rt: rt, log: &logger{debug}}
}

// httpLogger defines the interface to log http request and responses
type httpLogger interface {
	LogRequest(*http.Request)
	LogResponse(*http.Request, *http.Response, error, time.Duration)
}

type logger struct {
	*log.Logger
}

// LogRequest doens't do anything since we'll be logging replies only
func (dl *logger) LogRequest(*http.Request) {
}

// LogResponse logs path, host, status code and duration in milliseconds
func (dl *logger) LogResponse(req *http.Request, res *http.Response, err error, duration time.Duration) {
	duration /= time.Millisecond
	if err != nil {
		dl.Logger.Printf(">>> %s %s status=error durationMs=%d", req.Method, req.URL.String(), duration)
		dumpHeader(req.Header, dl.Logger)
		dl.Logger.Printf("<<< ERROR: %v", err.Error())
		dl.Logger.Printf("")
	} else {
		dl.Logger.Printf(">>> %s %s status=%d durationMs=%d", req.Method, req.URL.String(), res.StatusCode, duration)
		dumpHeader(req.Header, dl.Logger)
		dl.Logger.Printf("<<< %s %s", res.Proto, res.Status)
		dumpHeader(res.Header, dl.Logger)
	}
}

func dumpHeader(hdr http.Header, debug *log.Logger) {
	var buf bytes.Buffer
	hdr.Write(&buf) // nolint: errcheck
	scanner := bufio.NewScanner(&buf)
	for scanner.Scan() {
		debug.Printf("%s", scanner.Text())
	}
	debug.Printf("")
}
