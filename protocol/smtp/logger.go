package smtp

import (
	"log"
	"net"
)

type logConn struct {
	net.Conn
	log *log.Logger
}

func newLogConn(c net.Conn, log *log.Logger) *logConn {
	return &logConn{
		Conn: c,
		log:  log,
	}
}

func (l *logConn) Read(b []byte) (int, error) {
	n, err := l.Conn.Read(b)
	if err == nil {
		l.log.Printf("<<< %s", b[:n])
	}
	return n, err
}

func (l *logConn) Write(b []byte) (int, error) {
	n, err := l.Conn.Write(b)
	if err == nil {
		l.log.Printf(">>> %s", b[:n])
	}
	return n, err
}
