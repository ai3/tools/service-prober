package ssl

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
)

type SSLOptions struct {
	CAFile         string `json:"ca"`
	CertFile       string `json:"cert"`
	KeyFile        string `json:"key"`
	SkipValidation bool   `json:"skip_validation"`
	ServerName     string `json:"server_name"`
}

func ParseSSLOptions(opts *SSLOptions) (*tls.Config, error) {
	if opts == nil {
		return nil, nil
	}

	config := new(tls.Config)

	config.InsecureSkipVerify = opts.SkipValidation
	config.ServerName = opts.ServerName

	if opts.CertFile != "" && opts.KeyFile != "" {
		cert, err := tls.LoadX509KeyPair(opts.CertFile, opts.KeyFile)
		if err != nil {
			return nil, err
		}
		config.Certificates = []tls.Certificate{cert}
	}

	if opts.CAFile != "" {
		data, err := ioutil.ReadFile(opts.CAFile)
		if err != nil {
			return nil, err
		}
		cas := x509.NewCertPool()
		if !cas.AppendCertsFromPEM(data) {
			return nil, fmt.Errorf("no certificates could be parsed in %s", opts.CAFile)
		}
		config.RootCAs = cas
	}

	return config, nil
}
