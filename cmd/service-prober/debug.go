package main

import (
	"bufio"
	"fmt"
	"html/template"
	"net/http"
	"strings"
	"time"

	"git.autistici.org/ai3/tools/service-prober/probes"
)

var (
	debugHeadHTML = `<!DOCTYPE html>
<html lang="en">
<head>
  <title>Service Prober</title>
  <style type="text/css">
    body {
	background: white;
	font-family: sans-serif;
	width: 80%;
	margin: 0 auto;
    }
    a, a:visited {
	color: #00a;
	text-decoration: none;
    }
    a:hover {
	text-decoration: underline;
    }
    .error a {
	color: red;
    }
  </style>
</head>`

	debugHomeHTML = debugHeadHTML + `
<body>
  <h1>Service Prober</h1>
  <p>
  {{if .ErrsOnly}}
    <a href="/">Show all results</a>
  {{else}}
    <a href="/?errs=y">Show only errors</a>
  {{end}}
  </p>
  {{range .Results}}
  <p>
    <a href="/detail?id={{.ID}}"{{if not .Ok}} class="error"{{end}}>
      {{timefmt .Start}} / <b>{{if .Ok}}OK{{else}}ERROR{{end}}</b> / {{.Spec.Name}}</a>
    {{if not .Ok}}<br><small>{{.Error}}</small>{{end}}
  </p>
  {{end}}
</body>
</html>
`
	debugHomeTpl = template.Must(template.New("home").Funcs(map[string]interface{}{
		"timefmt": timefmt,
	}).Parse(debugHomeHTML))

	debugDetailsHTML = debugHeadHTML + `
<body>
  <h1>Probe {{.Spec.Name}} ({{.ID}}) / {{if .Ok}}OK{{else}}ERROR{{end}}</h1>
  <pre>{{.Logs}}</pre>
  <p>
    <a href="/">Back</a>
  </p>
</body>
`
	debugDetailsTpl = template.Must(template.New("details").Funcs(map[string]interface{}{
		"timefmt": timefmt,
	}).Parse(debugDetailsHTML))
)

type debugServer struct {
	store probes.ResultStore
}

func (d *debugServer) handleDebugHome(w http.ResponseWriter, req *http.Request) {
	if req.URL.Path != "/" {
		http.NotFound(w, req)
		return
	}

	var results []*probes.Result
	appendResult := func(r *probes.Result) {
		results = append(results, r)
	}
	errsOnly := (req.FormValue("errs") == "y")
	if errsOnly {
		d.store.EachErrs(appendResult)
	} else {
		d.store.Each(appendResult)
	}
	reverseOrder(results)

	debugHomeTpl.Execute(w, map[string]interface{}{
		"Results":  results,
		"ErrsOnly": errsOnly,
	})
}

func (d *debugServer) handleDebugDetails(w http.ResponseWriter, req *http.Request) {
	result := d.store.Find(req.FormValue("id"))
	if result == nil {
		http.NotFound(w, req)
		return
	}

	debugDetailsTpl.Execute(w, result)
}

func timefmt(t time.Time) string {
	return t.Format("2006/01/02 15:04:05")
}

// A ResultStore that dumps logs to standard output. Since the only
// purpose of this ResultStore is for one-shot mode, we don't even
// bother wrapping another ResultStore.
type dumpResultStore struct{}

func (s *dumpResultStore) Push(r *probes.Result) {
	// Prepend every line with the probe ID.
	scanner := bufio.NewScanner(strings.NewReader(r.Logs))
	for scanner.Scan() {
		fmt.Printf("%s: %s\n", r.ID, scanner.Text())
	}
}

func (s *dumpResultStore) Each(func(*probes.Result))     {}
func (s *dumpResultStore) EachErrs(func(*probes.Result)) {}
func (s *dumpResultStore) Find(string) *probes.Result    { return nil }

func reverseOrder(s []*probes.Result) {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
}
