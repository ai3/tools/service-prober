package serviceprober

import (
	"testing"

	"git.autistici.org/ai3/tools/service-prober/probes"
	_ "git.autistici.org/ai3/tools/service-prober/probes/imap"
)

func TestProbe_Parse(t *testing.T) {
	specJSON := `{
  "vars": {
    "credentials": [
      { "username": "uno", "password": "p1" },
      { "username": "due", "password": "p2" }
    ],
    "servers": [
      "1.2.3.4",
      "2.3.4.5"
    ]
  },
  "probes": [
    {
      "type": "imap_login",
      "name": "imap-login/${credentials.username}/${servers}",
      "loop": ["credentials", "servers"],
      "interval": "10s",
      "timeout": "10s",
      "labels": {"probeset": "custom"},
      "params": {
        "addr": "${servers}:993",
        "username": "${credentials.username}",
        "password": "${credentials.password}"
      }
    }
  ]
}`

	pp, err := probes.ParseConfig([]byte(specJSON), probes.NewResultStore(10, 10))
	if err != nil {
		t.Fatal(err)
	}
	if len(pp) != 4 {
		t.Fatalf("got %d probes, expecting 4", len(pp))
	}
}
