package vars

import "testing"

func TestVars(t *testing.T) {
	vars := map[string]interface{}{
		"alpha": "42",
		"beta":  "13",
		"gamma": "/path/to/gamma",
	}

	s, err := expandVars("the answer is ${alpha}", vars)
	if err != nil {
		t.Fatal(err)
	}
	if s != "the answer is 42" {
		t.Fatalf("bad result: '%s'", s)
	}

	_, err = expandVars("the answer is ${unknown_var}", vars)
	if err == nil {
		t.Fatal("no error on unknown variable")
	}
}

func TestVars_Nested(t *testing.T) {
	vars := map[string]interface{}{
		"alpha": map[string]interface{}{
			"up":   "42",
			"down": "21",
		},
		"beta":  "13",
		"gamma": "/path/to/gamma",
	}

	s, err := expandVars("the answer is ${alpha.up}", vars)
	if err != nil {
		t.Fatal(err)
	}
	if s != "the answer is 42" {
		t.Fatalf("bad result: '%s'", s)
	}

	_, err = expandVars("the answer is ${alpha.this.is.an.unknown_var}", vars)
	if err == nil {
		t.Fatal("no error on unknown variable")
	}
}

type testType1 struct {
	Banana     string
	MoreBanana string
	Attrs      map[string]string
	Type2      *testType2
	Slice      []string
}

type testType2 struct {
	Foo    string
	Inline testType3
}

type testType3 struct {
	B1 string
	B2 string
}

func TestVars_Obj(t *testing.T) {
	vars := map[string]interface{}{
		"alpha": map[string]interface{}{
			"up":   "42",
			"down": "21",
		},
		"beta":  "13",
		"gamma": "/path/to/gamma",
	}

	in := &testType1{
		Banana:     "the answer is ${alpha.up}",
		MoreBanana: "more banana",
		Attrs: map[string]string{
			"hey": "${alpha.down}",
		},
		Type2: &testType2{
			Foo: "${beta}",
			Inline: testType3{
				B1: "${alpha.up}", B2: "${alpha.down}",
			},
		},
		Slice: []string{
			"${alpha.up}",
			"${alpha.down}",
		},
	}

	result, err := Expand(in, vars)
	if err != nil {
		t.Fatal(err)
	}
	out := result.(*testType1)

	if out.Banana != "the answer is 42" {
		t.Fatalf("bad result: %+v", out)
	}
	if out.MoreBanana != "more banana" {
		t.Fatalf("mangled result: %+v", out)
	}
	if s := out.Attrs["hey"]; s != "21" {
		t.Fatalf("no map[] recursion: %s", s)
	}
	if out.Type2.Foo != "13" {
		t.Fatalf("no struct ptr recursion: %v", out.Type2)
	}
	if out.Type2.Inline.B1 != "42" {
		t.Fatalf("no struct recursion: %v", out.Type2.Inline.B1)
	}
	if len(out.Slice) != 2 {
		t.Fatalf("output slice has wrong size %d, expecting 2", len(out.Slice))
	}
	if out.Slice[0] != "42" {
		t.Fatalf("no slice recursion: %v", out.Slice)
	}
}

func TestVars_CrossProduct(t *testing.T) {
	vars := map[string]interface{}{
		"alpha": []string{"1", "2", "3"},
		"gamma": []string{"a", "b"},
		"beta":  "42",
	}

	// Test simple iteration.
	iterVars, err := CrossProduct([]string{"alpha"}, vars)
	if err != nil {
		t.Fatal(err)
	}
	if len(iterVars) != 3 {
		t.Fatalf("crossProduct(a) returned %d items, expected 3", len(iterVars))
	}
	if s := iterVars[0]["alpha"].(string); s != "1" {
		t.Fatalf("crossProduct(a) returned bad expansion for first item: got '%s', expected 1", s)
	}
	if s := iterVars[1]["alpha"].(string); s != "2" {
		t.Fatalf("crossProduct(a) returned bad expansion for second item: got '%s', expected 2", s)
	}
	if s := iterVars[2]["alpha"].(string); s != "3" {
		t.Fatalf("crossProduct(a) returned bad expansion for third item: got '%s', expected 3", s)
	}
	if s := iterVars[0]["beta"]; s != "42" {
		t.Fatalf("crossProduct(a) mangled non-iterator variable beta: '%s'", s)
	}

	// Test cross-product between two iterators.
	iterVars, err = CrossProduct([]string{"alpha", "gamma"}, vars)
	if err != nil {
		t.Fatal(err)
	}
	if len(iterVars) != 6 {
		t.Fatalf("crossProduct(g) returned %d items, expected 6", len(iterVars))
	}
	if s := iterVars[0]["alpha"].(string); s != "1" {
		t.Fatalf("crossProduct(g) returned bad expansion for first item: alpha='%s', expected 1", s)
	}
	if s := iterVars[0]["gamma"].(string); s != "a" {
		t.Fatalf("crossProduct(g) returned bad expansion for first item: gamma='%s', expected 1", s)
	}

	// Test iteration on more complex objects.
	vars = map[string]interface{}{
		"alpha": []map[string]interface{}{
			map[string]interface{}{
				"up": "1",
			},
			map[string]interface{}{
				"up": "2",
			},
		},
	}
	iterVars, err = CrossProduct([]string{"alpha"}, vars)
	if err != nil {
		t.Fatal(err)
	}
	if len(iterVars) != 2 {
		t.Fatalf("crossProduct(m) returned %d items, expected 2", len(iterVars))
	}
	if s := iterVars[0]["alpha"].(map[string]interface{})["up"]; s != "1" {
		t.Fatalf("crossProduct(m) returned bad result: %+v", iterVars)
	}
}
