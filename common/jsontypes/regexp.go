package jsontypes

import (
	"encoding/json"
	"regexp"
)

// A regular expression that is compiled when unmarshaling it from JSON.
type Regexp struct {
	*regexp.Regexp
}

func (r *Regexp) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	comp, err := regexp.Compile(s)
	if err != nil {
		return err
	}
	r.Regexp = comp
	return nil
}

func (r Regexp) IsSet() bool {
	return r.Regexp != nil
}
