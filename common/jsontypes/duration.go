package jsontypes

import (
	"encoding/json"
	"time"
)

// Duration is a time.Duration that can be unmarshaled from JSON by
// calling time.ParseDuration().
type Duration struct {
	time.Duration
}

func (d *Duration) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	dd, err := time.ParseDuration(s)
	if err != nil {
		return err
	}
	d.Duration = dd
	return nil
}
