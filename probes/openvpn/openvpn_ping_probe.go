package openvpn

import (
	"context"
	"encoding/json"
	"errors"
	"log"
	"os"
	"time"

	"git.autistici.org/ai3/tools/service-prober/common/vars"
	"git.autistici.org/ai3/tools/service-prober/probes"
	"git.autistici.org/ai3/tools/service-prober/protocol/openvpn"

	"github.com/ooni/minivpn/vpn"
)

const (
	pingCount           = 5
	extraTimeoutSeconds = 10
)

func timeoutSecondsFromCount(count int) time.Duration {
	waitOnLastOne := time.Duration(5) * time.Second
	return time.Duration(count)*time.Second + waitOnLastOne

}

type openVPNProbeSpec struct {
	Remote     string `json:"remote"`
	Proto      string `json:"proto"`
	Port       string `json:"port"`
	Cipher     string `json:"cipher"`
	Auth       string `json:"auth"`
	PingTarget string `json:"pingTarget"`
	Cert       string `json:"cert"`
	Key        string `json:"key"`
	Ca         string `json:"ca"`
}

func parseOpenVPNProbeSpec(params json.RawMessage) (probes.Spec, error) {
	var spec openVPNProbeSpec
	err := json.Unmarshal(params, &spec)
	return &spec, err
}

func (spec *openVPNProbeSpec) Build(lookup map[string]interface{}) (probes.ProbeImpl, error) {
	expanded, err := vars.Expand(spec, lookup)
	if err != nil {
		return nil, err
	}
	s := expanded.(*openVPNProbeSpec)

	// Sanity checks.
	if s.Remote == "" {
		return nil, errors.New("remote is unset")
	}
	if s.Cipher == "" {
		return nil, errors.New("cipher is unset")
	}
	if s.Auth == "" {
		return nil, errors.New("auth is unset")
	}
	if s.Cert == "" {
		return nil, errors.New("cert is unset")
	}
	if s.Key == "" {
		return nil, errors.New("key is unset")
	}
	if s.Ca == "" {
		return nil, errors.New("ca is unset")
	}
	if !fileExists(s.Cert) {
		return nil, errors.New("cert path not found")
	}
	if !fileExists(s.Key) {
		return nil, errors.New("key path not found")
	}
	if !fileExists(s.Ca) {
		return nil, errors.New("ca path not found")
	}

	var vpnProto int
	switch s.Proto {
	case "udp":
		vpnProto = vpn.UDPMode
	case "tcp":
		vpnProto = vpn.TCPMode
	default:
		return nil, errors.New("unknown proto")
	}

	return &openVPNProbe{
		pingTarget: s.PingTarget,
		remote:     s.Remote,
		proto:      s.Proto,
		vpnProto:   vpnProto,
		port:       s.Port,
		cipher:     s.Cipher,
		auth:       s.Auth,
		key:        s.Key,
		cert:       s.Cert,
		ca:         s.Ca,
	}, nil
}

type openVPNProbe struct {
	pingTarget string
	remote     string
	proto      string
	vpnProto   int
	port       string
	cipher     string
	auth       string
	cert       string
	key        string
	ca         string
}

func (p *openVPNProbe) RunProbe(ctx context.Context, debug *log.Logger) error {
	debug.Printf("making OpenVPN connection to %s (%s/%s)", p.remote, p.port, p.proto)

	opts := &vpn.Options{
		Auth:   p.auth,
		Cipher: p.cipher,
		Port:   p.port,
		Remote: p.remote,
		Proto:  p.vpnProto,
		Cert:   []byte(p.cert),
		Key:    []byte(p.key),
		Ca:     []byte(p.ca),
	}

	timeout := timeoutSecondsFromCount(pingCount) + extraTimeoutSeconds

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	pingOpts := &openvpn.PingOptions{
		Target:  p.pingTarget,
		Count:   pingCount,
		Timeout: timeout,
	}

	pinger, err := openvpn.NewPingerFromOptions(opts, ctx, pingOpts)
	if err != nil {
		return err
	}
	err = pinger.Run(ctx)
	if err != nil {
		return err
	}
	debug.Printf("loss: %d%%", pinger.PacketLoss())

	if pinger.PacketLoss() > 50 {
		return errors.New("packet loss too high")
	}

	debug.Printf("success")
	return nil
}

func init() {
	probes.RegisterProbeType("openvpn_ping", parseOpenVPNProbeSpec)
}

func fileExists(name string) bool {
	_, err := os.Stat(name)
	return err == nil
}
