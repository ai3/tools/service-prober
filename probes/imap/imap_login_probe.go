package imap

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"log"

	"git.autistici.org/ai3/tools/service-prober/common/vars"
	"git.autistici.org/ai3/tools/service-prober/probes"
	"git.autistici.org/ai3/tools/service-prober/protocol/imap"
	"git.autistici.org/ai3/tools/service-prober/protocol/ssl"
)

type imapLoginProbeSpec struct {
	Addr     string            `json:"addr"`
	SSLOpts  *ssl.SSLOptions   `json:"ssl"`
	DNSMap   map[string]string `json:"dns_map"`
	Username string            `json:"username"`
	Password string            `json:"password"`
}

func parseIMAPLoginProbeSpec(params json.RawMessage) (probes.Spec, error) {
	var spec imapLoginProbeSpec
	err := json.Unmarshal(params, &spec)
	return &spec, err
}

func (spec *imapLoginProbeSpec) Build(lookup map[string]interface{}) (probes.ProbeImpl, error) {
	expanded, err := vars.Expand(spec, lookup)
	if err != nil {
		return nil, err
	}
	s := expanded.(*imapLoginProbeSpec)

	// Sanity checks.
	if s.Addr == "" {
		return nil, errors.New("addr is unset")
	}
	if s.Username == "" {
		return nil, errors.New("username is unset")
	}

	sslOpts, err := ssl.ParseSSLOptions(s.SSLOpts)
	if err != nil {
		return nil, err
	}

	return &imapLoginProbe{
		addr:     s.Addr,
		username: s.Username,
		password: s.Password,
		dnsMap:   s.DNSMap,
		tls:      sslOpts,
	}, nil
}

type imapLoginProbe struct {
	addr     string
	username string
	password string
	dnsMap   map[string]string
	tls      *tls.Config
}

func (p *imapLoginProbe) RunProbe(ctx context.Context, debug *log.Logger) error {
	debug.Printf("making IMAP connection to %s", p.addr)
	conn, err := imap.Dial(ctx, p.addr, p.tls, debug, p.dnsMap)
	if err != nil {
		return err
	}
	defer conn.Close()

	debug.Printf("logging in as %s", p.username)
	if err := conn.LoginSASL(p.username, p.password); err != nil {
		return err
	}

	debug.Printf("opening INBOX")
	if err := conn.Select("INBOX"); err != nil {
		return err
	}

	debug.Printf("success")
	return nil
}

func init() {
	probes.RegisterProbeType("imap_login", parseIMAPLoginProbeSpec)
}
