package probes

import (
	"reflect"
	"testing"
)

func TestResultBuffer(t *testing.T) {
	b := newResultBuffer(3)
	b.Push(&Result{ID: "1"})
	b.Push(&Result{ID: "2"})
	b.Push(&Result{ID: "3"})
	b.Push(&Result{ID: "4"})

	var ids []string
	b.Each(func(r *Result) {
		ids = append(ids, r.ID)
	})

	if !reflect.DeepEqual(ids, []string{"2", "3", "4"}) {
		t.Fatalf("unexpected buffer contents: %v", ids)
	}
}
