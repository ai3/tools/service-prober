package http

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"log"

	"git.autistici.org/ai3/tools/service-prober/common/vars"
	"git.autistici.org/ai3/tools/service-prober/probes"
	"git.autistici.org/ai3/tools/service-prober/protocol/http"
	"git.autistici.org/ai3/tools/service-prober/protocol/ssl"
)

type httpProbeSpec struct {
	SSLOpts *ssl.SSLOptions   `json:"ssl"`
	DNSMap  map[string]string `json:"dns_map"`
	Script  []*http.Step      `json:"script"`
}

func parseHTTPProbeSpec(params json.RawMessage) (probes.Spec, error) {
	var spec httpProbeSpec
	err := json.Unmarshal(params, &spec)
	return &spec, err
}

func (spec *httpProbeSpec) Build(lookup map[string]interface{}) (probes.ProbeImpl, error) {
	expanded, err := vars.Expand(spec, lookup)
	if err != nil {
		return nil, err
	}
	s := expanded.(*httpProbeSpec)

	sslOpts, err := ssl.ParseSSLOptions(s.SSLOpts)
	if err != nil {
		return nil, err
	}

	return &httpProbe{
		tls:    sslOpts,
		script: s.Script,
		dnsMap: s.DNSMap,
	}, nil
}

type httpProbe struct {
	script []*http.Step
	tls    *tls.Config
	dnsMap map[string]string
}

func (p *httpProbe) RunProbe(ctx context.Context, debug *log.Logger) error {
	browser := http.NewBrowser(p.tls, debug, p.dnsMap)
	for idx, step := range p.script {
		stepName := fmt.Sprintf("#%d", idx+1)
		if step.Name != "" {
			stepName = fmt.Sprintf("%s - '%s'", stepName, step.Name)
		}
		debug.Printf("sequence step %s", stepName)
		if err := http.CheckStep(ctx, browser, step, debug); err != nil {
			err = fmt.Errorf("step %s failed: %w", stepName, err)
			debug.Printf("%v", err)
			browser.Dump(debug)
			return err
		}
	}
	return nil
}

func init() {
	probes.RegisterProbeType("http", parseHTTPProbeSpec)
}
