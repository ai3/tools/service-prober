package scheduler

import (
	"context"
	"sync/atomic"
	"testing"
	"time"
)

type testEvent struct {
	counter  *uint32
	interval time.Duration
}

func (t *testEvent) Interval() time.Duration { return t.interval }
func (t *testEvent) Tick(ctx context.Context) {
	atomic.AddUint32(t.counter, 1)
}

func TestScheduler(t *testing.T) {
	s := New()

	var counter1, counter2 uint32

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	go func() {
		s.Add(&testEvent{counter: &counter1, interval: 100 * time.Millisecond})
		s.Add(&testEvent{counter: &counter2, interval: 180 * time.Millisecond})
	}()
	s.Run(ctx)
	cancel()

	// There is a bit of leeway depending on the initial random
	// offset since it might be smaller than the execution time of
	// time.Sleep().
	if counter1 < 10 || counter1 > 11 {
		t.Errorf("counter1 is %d, expected 10", counter1)
	}
	if counter2 != 6 {
		t.Errorf("counter2 is %d, expected 8", counter2)
	}
}
