package scheduler

import (
	"container/heap"
	"context"
	"math/rand"
	"sync"
	"time"
)

// A Scheduler runs periodic events.
type Scheduler struct {
	heap       timerHeap
	scheduleCh chan *timerData
}

func New() *Scheduler {
	return &Scheduler{
		scheduleCh: make(chan *timerData, 1),
	}
}

func (s *Scheduler) Add(event PeriodicEvent) {
	// Schedule the task at some random point in the future during its
	// first interval, so that all tasks are scattered equally.
	intervalFrac := time.Duration(event.Interval().Seconds()*rand.Float64()) * time.Second
	when := time.Now().Add(intervalFrac)
	s.scheduleCh <- &timerData{
		event: event,
		time:  when,
	}
}

func (s *Scheduler) Run(ctx context.Context) {
	// Avoid closing the channel before all goroutines have completed, or
	// we risk a panic due to send on closed channel. This still isn't a
	// clean exit, and will sometimes result in a data race.
	var wg sync.WaitGroup
	go func() {
		<-ctx.Done()
		wg.Wait()
		close(s.scheduleCh)
	}()

	var timer *time.Timer
	var timeCh <-chan time.Time

	for {
		var t time.Time
		select {
		case data := <-s.scheduleCh:
			if data == nil {
				return
			}
			heap.Push(&s.heap, data)
			if timer != nil {
				timer.Stop()
			}
			t = time.Now()

		case t = <-timeCh:
			data := heap.Pop(&s.heap).(*timerData)
			wg.Add(1)
			go func() {
				data.event.Tick(ctx)

				// Reschedule the event.
				s.scheduleCh <- &timerData{
					event: data.event,
					time:  nextTime(t, data.time, data.event.Interval()),
				}

				wg.Done()
			}()
		}

		timer = nil
		timeCh = nil
		if len(s.heap) > 0 {
			// It is important that we can trigger an immediate
			// timer event if the 'next' timestamp is in the past.
			next := s.heap[0].time
			timer = time.NewTimer(next.Sub(t))
			timeCh = timer.C
		}
	}
}

// A PeriodicEvent is a repeatable event with a fixed interval.
type PeriodicEvent interface {
	Tick(context.Context)
	Interval() time.Duration
}

type timerData struct {
	event PeriodicEvent
	time  time.Time
	index int
}

type timerHeap []*timerData

func (h timerHeap) Len() int {
	return len(h)
}

func (h timerHeap) Less(i, j int) bool {
	return h[i].time.Before(h[j].time)
}

func (h timerHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
	h[i].index, h[j].index = i, j
}

func (h *timerHeap) Push(x interface{}) {
	data := x.(*timerData)
	*h = append(*h, data)
	data.index = len(*h) - 1
}

func (h *timerHeap) Pop() interface{} {
	n := len(*h)
	data := (*h)[n-1]
	*h = (*h)[:n-1]
	data.index = -1
	return data
}

func nextTime(now, cur time.Time, interval time.Duration) time.Time {
	// Math is hard, let's waste CPU cycles.
	for cur.Before(now) {
		cur = cur.Add(interval)
	}
	return cur
}
